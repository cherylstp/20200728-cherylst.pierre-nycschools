package jpmc.coding_challenge.jpmc_20200728_cherylstpierre_nycschools.viewModels;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import jpmc.coding_challenge.jpmc_20200728_cherylstpierre_nycschools.models.SchoolData;
import jpmc.coding_challenge.jpmc_20200728_cherylstpierre_nycschools.networking.callbacks.GenericListCallback;
import jpmc.coding_challenge.jpmc_20200728_cherylstpierre_nycschools.repositories.SchoolRepo;

public class SchoolListViewModel extends AndroidViewModel {
    private SchoolRepo schoolRepo;
    private MutableLiveData<List<SchoolData>> schoolData;
    private MutableLiveData<String> message;

    public SchoolListViewModel(Application app){
        super(app);
        schoolRepo = SchoolRepo.getInstance(app);
    }

    public LiveData<List<SchoolData>> getSchoolData(){
        if(schoolData == null){
            schoolData = new MutableLiveData<>();
        }
        return schoolData;
    }

    public LiveData<String> getMessage(){
        if(message == null){
            message = new MutableLiveData<>();
        }
        return message;
    }

    public void getSchoolDataFromApi(){
        schoolRepo.getAllSchoolData(new GenericListCallback() {
            @Override
            public <T> void handleListResponse(List<T> list, String error) {
                if(error == null){
                    schoolData.setValue((List<SchoolData>)list);
                }
                else{
                    // This will only ever be the generic error string that we set,
                    // so we don't have to worry about displaying anything like a
                    // stack trace to the user
                    message.setValue(error);
                }
            }
        });
    }

}
