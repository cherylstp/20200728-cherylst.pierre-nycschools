package jpmc.coding_challenge.jpmc_20200728_cherylstpierre_nycschools.viewModels;

import android.app.Application;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import jpmc.coding_challenge.jpmc_20200728_cherylstpierre_nycschools.models.SATData;
import jpmc.coding_challenge.jpmc_20200728_cherylstpierre_nycschools.models.SchoolData;
import jpmc.coding_challenge.jpmc_20200728_cherylstpierre_nycschools.networking.callbacks.GenericCallback;
import jpmc.coding_challenge.jpmc_20200728_cherylstpierre_nycschools.repositories.SchoolRepo;

public class SchoolDetailViewModel extends AndroidViewModel {
    private SchoolRepo schoolRepo;
    private String schoolName;
    private MutableLiveData<SATData> satData;
    private MutableLiveData<String> message;
    private SchoolData data;

    public SchoolDetailViewModel(@NonNull Application application) {
        super(application);
        schoolRepo = SchoolRepo.getInstance(application);
    }

    public SchoolData getData() {
        return data;
    }

    public void setData(SchoolData data) {
        this.data = data;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public LiveData<SATData> getSATData(){
        if(satData == null){
            satData = new MutableLiveData<>();
        }
        return satData;
    }

    public LiveData<String> getMessage(){
        if(message == null){
            message = new MutableLiveData<>();
        }
        return message;
    }

    public void getSATDataFromAPI(){
        schoolRepo.getAllSATData(data.getDbn(), new GenericCallback() {
            @Override
            public <T> void handleResponse(T model, String error) {
                if(error == null){
                    // So this endpoint returns a list of SATdata, but we're querying via the API
                    // so we know this will only ever return one item in the list
                    // given more time, I would have abstracted this piece to the repo function
                    // so that way this function only returns one object
                    List<SATData> list = (List<SATData>)model;
                    if(list == null || list.size() == 0){
                        satData.setValue(null);
                    }
                    else {
                        satData.setValue(list.get(0));
                    }
                }
                else{
                    message.setValue(error);
                }
            }
        });
    }

    public void handleArgs(Bundle bundle){
        String schoolName = bundle.getString("school_name");
        String overview = bundle.getString("overview");
        String dbn = bundle.getString("dbn");
        String grades = bundle.getString("grades");
        String eligibility = bundle.getString("eligibility");
        String interest = bundle.getString("interest");
        String admissions1 = bundle.getString("admissions1");
        String admissions2 = bundle.getString("admissions2");
        String extracurr = bundle.getString("extracurr");
        String totalStudents = bundle.getString("totalStudents");
        String addtl = bundle.getString("addtl");
        SchoolData schoolData = new SchoolData(schoolName, overview, dbn, eligibility, interest, addtl, admissions1, admissions2, grades,
                totalStudents, extracurr);
        setData(schoolData);
    }
}
