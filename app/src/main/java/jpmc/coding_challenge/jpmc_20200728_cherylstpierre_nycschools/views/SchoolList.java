package jpmc.coding_challenge.jpmc_20200728_cherylstpierre_nycschools.views;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.google.android.material.snackbar.Snackbar;

import java.util.List;

import jpmc.coding_challenge.jpmc_20200728_cherylstpierre_nycschools.R;
import jpmc.coding_challenge.jpmc_20200728_cherylstpierre_nycschools.databinding.FragmentSchoolListBinding;
import jpmc.coding_challenge.jpmc_20200728_cherylstpierre_nycschools.models.SchoolData;
import jpmc.coding_challenge.jpmc_20200728_cherylstpierre_nycschools.viewModels.SchoolListViewModel;
import jpmc.coding_challenge.jpmc_20200728_cherylstpierre_nycschools.views.adapters.SchoolListExpandableAdapter;

/***
 * The design is a bit plain, but I wanted to focus my time on organizing the data rather than dumping
 * all 500+ schools into one RecyclerView. This design gives a bit of sorting by organizing the
 * schools by borough, to give the user the ability to view by location. Given more time I would have
 * loved to implement more sorting methods - that way the user can really get the schools that they're
 * looking for as fast as possible. This could include: filters for grades, requirements, interests, etc
 * on top of the expandable list view. (and of course a theme with material design principles implemented)
 */
public class SchoolList extends Fragment {
    SchoolListViewModel viewModel;
    FragmentSchoolListBinding binding;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new ViewModelProvider(this).get(SchoolListViewModel.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentSchoolListBinding.inflate(getLayoutInflater());
        setObservers();
        viewModel.getSchoolDataFromApi();
        return binding.getRoot();
    }

    /*@Override
    public void onResume() {
        super.onResume();
        if(viewModel.getSchoolData().getValue() != null){
            toggleProgressBar(false);
        }
    }*/

    private void setObservers(){
        viewModel.getSchoolData().observe(getViewLifecycleOwner(), schoolData ->{
            setUpExpandableListView(schoolData);
            toggleProgressBar(false);

        });
        viewModel.getMessage().observe(getViewLifecycleOwner(), message ->{
            Snackbar.make(getView(), message, Snackbar.LENGTH_SHORT).show();
        });
    }

    private void setUpExpandableListView(List<SchoolData>schoolData){
        SchoolListExpandableAdapter adapter = new SchoolListExpandableAdapter(getActivity().getApplication(),
                schoolData);
        binding.schoolELV.setAdapter(adapter);
        binding.schoolELV.setOnChildClickListener((parent, v, groupPosition, childPosition, id) -> {
            SchoolData data = (SchoolData) adapter.getChild(groupPosition, childPosition);
            Bundle bundle = new Bundle();
            /*
             * Typically I don't love to pass data this way between fragments, since there's quite a few fields
             * here that we need. I would usually have a Room database implemented, and would pass the
             * primary key and run a query to get the object in the next associated viewmodel.
             * However, this app didn't seem to warrant a database given that we aren't saving any kind of
             * data, and I wouldn't want to make the app larger than it needs to be. I understand
             * that users are often concerned about the size of the apps they download
             */
            // given more time, I would add these strings to the string resource file
            bundle.putString("school_name", data.getSchool_name());
            bundle.putString("overview", data.getOverview_paragraph());
            bundle.putString("dbn", data.getDbn());
            bundle.putString("grades", data.getFinalgrades());
            bundle.putString("eligibility", data.getEligibility1());
            bundle.putString("interest", data.getInterest1());
            bundle.putString("admissions1", data.getAdmissionspriority11());
            bundle.putString("admissions2", data.getAdmissionspriority21());
            bundle.putString("extracurr", data.getExtracurricular_activities());
            bundle.putString("totalStudents", data.getTotal_students());
            bundle.putString("addtl", data.getAddtl_info1());
            Navigation.findNavController(getView()).navigate(R.id.action_schoolList_to_schoolDetail, bundle);
            return false;
        });
    }

    private void toggleProgressBar(boolean showProgressBar){
        if(showProgressBar){
            binding.schoolELV.setVisibility(View.GONE);
            binding.progressBar.setVisibility(View.VISIBLE);
        }
        else{
            binding.progressBar.setVisibility(View.GONE);
            binding.schoolELV.setVisibility(View.VISIBLE);
        }
    }
}