package jpmc.coding_challenge.jpmc_20200728_cherylstpierre_nycschools.views;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.snackbar.Snackbar;

import jpmc.coding_challenge.jpmc_20200728_cherylstpierre_nycschools.R;
import jpmc.coding_challenge.jpmc_20200728_cherylstpierre_nycschools.databinding.FragmentSchoolDetailBinding;
import jpmc.coding_challenge.jpmc_20200728_cherylstpierre_nycschools.viewModels.SchoolDetailViewModel;

/***
 * Given more time, I would have loved to use the lat / long coords to show a map to the user
 * (and taping on the map would have launched the Google Maps app. I would have also liked to add a
 * 'Requirements' section, though I would need more time to take a closer look at the data
 * and see which fields are commonly available; I found that the data between each object
 * can vary a LOT.
 * The theme is a bit bare-bones, and given more time I would have implemented a set theme with material design principles
 * to make everything look a bit prettier and modern. However, I wanted to focus the bulk of my time on showing clean code
 * and my ability to work with API endpoints and LiveData, as well as keeping to the MVVM design pattern.
 */
public class SchoolDetail extends Fragment {
    SchoolDetailViewModel viewModel;
    FragmentSchoolDetailBinding binding;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new ViewModelProvider(this).get(SchoolDetailViewModel.class);
        if(getArguments() != null){
            viewModel.handleArgs(getArguments());
            viewModel.getSATDataFromAPI();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentSchoolDetailBinding.inflate(getLayoutInflater());
        binding.setSchool(viewModel.getData());
        setObservers();
        return binding.getRoot();
    }

    private void setObservers(){
        viewModel.getSATData().observe(getViewLifecycleOwner(), satData -> {
            binding.setSATData(satData);
        });
        viewModel.getMessage().observe(getViewLifecycleOwner(), message ->{
            Snackbar.make(getView(), message, Snackbar.LENGTH_SHORT).show();
        });
    }
}