package jpmc.coding_challenge.jpmc_20200728_cherylstpierre_nycschools.views.adapters;

import android.app.Application;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;

import com.google.android.material.textview.MaterialTextView;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

import jpmc.coding_challenge.jpmc_20200728_cherylstpierre_nycschools.R;
import jpmc.coding_challenge.jpmc_20200728_cherylstpierre_nycschools.databinding.AdapterGroupItemBinding;
import jpmc.coding_challenge.jpmc_20200728_cherylstpierre_nycschools.databinding.AdapterSchoolItemBinding;
import jpmc.coding_challenge.jpmc_20200728_cherylstpierre_nycschools.models.SchoolData;

public class SchoolListExpandableAdapter extends BaseExpandableListAdapter {
    List<String> parentGroup;
    List<SchoolData> schoolData;
    TreeMap<String, List<SchoolData>> schoolMap;
    private String manhattan;
    private String queens;
    private String brooklyn;
    private String bronx;
    private String statenIsland;
    private String statenIslandCompare;
    private String notListed;
    private String NA;

    public SchoolListExpandableAdapter(Application app, List<SchoolData> schoolData){
        this.schoolData = schoolData;
        manhattan = app.getString(R.string.manhattan);
        queens = app.getString(R.string.queens);
        brooklyn = app.getString(R.string.brooklyn);
        bronx = app.getString(R.string.bronx);
        statenIsland = app.getString(R.string.staten_island);
        statenIslandCompare = app.getString(R.string.staten_island_compare);
        notListed = app.getString(R.string.no_borough);
        NA = app.getString(R.string.NA);
        populateParentGroup();
        populateSchoolMap();
    }

    private void populateParentGroup(){
        parentGroup = new ArrayList<>();
        // Since we know the values we can expect here, we can manually add these
        // If we didn't know them ahead of time, we could populate this dynamically
        parentGroup.add(manhattan);
        parentGroup.add(queens);
        parentGroup.add(brooklyn);
        parentGroup.add(bronx);
        parentGroup.add(statenIsland);
        parentGroup.sort((o1, o2) -> o1.compareTo(o2));
        parentGroup.add(notListed);
    }

    private void populateSchoolMap(){
        // Again, since we know we have only 5 options here, we can add manually
        // if not, we would add to the group dynamically
        schoolMap = new TreeMap<>();
        schoolMap.put(manhattan, new ArrayList<>());
        schoolMap.put(queens, new ArrayList<>());
        schoolMap.put(brooklyn, new ArrayList<>());
        schoolMap.put(bronx, new ArrayList<>());
        schoolMap.put(statenIsland, new ArrayList<>());
        schoolMap.put(notListed, new ArrayList<>());

        for(SchoolData data : schoolData){
            String borough = data.getBorough();
            if(borough == null){
                schoolMap.get(notListed).add(data);
            }
            else{
                borough = borough.trim();
                if(borough.equals(manhattan.toUpperCase())){
                    schoolMap.get(manhattan).add(data);
                }
                else if(borough.equals(queens.toUpperCase())){
                    schoolMap.get(queens).add(data);
                }
                else if(borough.equals(brooklyn.toUpperCase())){
                    schoolMap.get(brooklyn).add(data);
                }
                else if(borough.equals(bronx.toUpperCase())){
                    schoolMap.get(bronx).add(data);
                }
                else if(borough.equals(statenIslandCompare.toUpperCase())){
                    schoolMap.get(statenIsland).add(data);
                }
            }
        }
    }

    @Override
    public int getGroupCount() {
        return parentGroup.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        List<SchoolData> childList = schoolMap.get(parentGroup.get(groupPosition));
        if(childList == null){
            return 0;
        }
        else{
            return childList.size();
        }
    }

    @Override
    public Object getGroup(int groupPosition) {
        return parentGroup.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        List<SchoolData> childList = schoolMap.get(parentGroup.get(groupPosition));
        if(childList == null){
            return null;
        }
        else{
            return childList.get(childPosition);
        }
    }

    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        GroupViewHolder viewHolder;
        if(convertView == null){
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            AdapterGroupItemBinding binding = AdapterGroupItemBinding.inflate(inflater, null, false);
            viewHolder = new GroupViewHolder(binding);
            viewHolder.groupItemBinding.getRoot().setTag(viewHolder);
        }
        else{
            viewHolder = (GroupViewHolder) convertView.getTag();
        }
        viewHolder.groupItemBinding.parentTV.setText((String)getGroup(groupPosition));
        return viewHolder.groupItemBinding.getRoot();
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        ItemViewHolder viewHolder;
        SchoolData data = (SchoolData)getChild(groupPosition, childPosition);
        if(convertView == null){
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            AdapterSchoolItemBinding binding = AdapterSchoolItemBinding.inflate(inflater, null, false);
            viewHolder = new ItemViewHolder(binding);
            viewHolder.binding.getRoot().setTag(viewHolder);
        }
        else{
            viewHolder = (ItemViewHolder) convertView.getTag();
        }
        viewHolder.bind(data);
        if(data.getInterest1() == null){
            viewHolder.binding.interest.setText(NA);
        }
        if(data.getBorough() == null){
            viewHolder.binding.borough.setText(NA);
        }
        return viewHolder.binding.getRoot();
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    // View Holder classes to which we bind the UI elements
    // Doing this allows for faster access to the UI, and is less intensive than
    // using findViewById
    static class GroupViewHolder {
        AdapterGroupItemBinding groupItemBinding;
        MaterialTextView groupTV;

        public GroupViewHolder(AdapterGroupItemBinding groupItemBinding) {
            this.groupItemBinding = groupItemBinding;
            groupTV = groupItemBinding.parentTV;
        }
    }

    static class ItemViewHolder {
        AdapterSchoolItemBinding binding;

        public ItemViewHolder(AdapterSchoolItemBinding binding) {
            this.binding = binding;
        }

        public void bind(SchoolData data) {
            binding.setSchool(data);
            binding.executePendingBindings();
        }
    }
}

