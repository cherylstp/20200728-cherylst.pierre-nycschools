package jpmc.coding_challenge.jpmc_20200728_cherylstpierre_nycschools.networking;

import java.util.List;

import jpmc.coding_challenge.jpmc_20200728_cherylstpierre_nycschools.models.SATData;
import jpmc.coding_challenge.jpmc_20200728_cherylstpierre_nycschools.models.SchoolData;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface Routes {
    @GET("resource/s3k6-pzi2.json")
    Call<List<SchoolData>> getAllSchools();

    @GET("resource/f9bf-2cp4.json")
    Call<List<SATData>> getAllSATData(@Query("dbn") String dbn);

}
