package jpmc.coding_challenge.jpmc_20200728_cherylstpierre_nycschools.networking;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitServiceGenerator {
    private static Retrofit retrofit;
    private static final String SITE_ROOT = "https://data.cityofnewyork.us/";


    private static Retrofit.Builder builder =
            new Retrofit.Builder()
                    .baseUrl(SITE_ROOT)
                    .addConverterFactory(GsonConverterFactory.create());

    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

    // In this function, I would typically include a username and password in the constructor
    // and pass that to a separate class to handle creating an authentication interceptor.
    // Since this is a simple app and doesn't require a login, there is no need
    public static <S> S createService(Class<S> serviceClass) {
            if(httpClient == null){
                httpClient = new OkHttpClient.Builder()
                        .retryOnConnectionFailure(true);
            }
            retrofit = builder.build();
            httpClient.interceptors().clear();
            return retrofit.create(serviceClass);
    }
}
