package jpmc.coding_challenge.jpmc_20200728_cherylstpierre_nycschools.networking.callbacks;

public interface GenericCallback {
    <T> void handleResponse(T model, String error);
}
