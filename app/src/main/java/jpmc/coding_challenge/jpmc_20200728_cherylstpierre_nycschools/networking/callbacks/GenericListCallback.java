package jpmc.coding_challenge.jpmc_20200728_cherylstpierre_nycschools.networking.callbacks;

import java.util.List;

public interface GenericListCallback {
    <T> void handleListResponse(List<T> list, String error);
}
