package jpmc.coding_challenge.jpmc_20200728_cherylstpierre_nycschools.models;

public class SchoolData {

    /**
     * There are a LOT of columns for this model, so only took the data that seemed
     * relevant to the app.
     */
    String dbn;
    String school_name;
    String boro;
    String overview_paragraph;
    String academicopportunities1;
    String academicopportunities2;
    String ell_programs;
    String neighborhood;
    String building_code;
    String location;
    String phone_number;
    String fax_number;
    String school_email;
    String website;
    String subway;
    String bus;
    String grades2018;
    String finalgrades;
    String total_students;
    String extracurricular_activities;
    String school_sports;
    String attendance_rate;
    String pct_stu_enough_variety;
    String pct_stu_safe;
    String school_accessibility_description;
    String directions1;
    String requirement1_1;
    String requirement2_1;
    String requirement3_1;
    String requirement4_1;
    String requirement5_1;
    String eligibility1;
    String program1;
    String interest1;
    String admissionspriority11;
    String admissionspriority21;
    String admissionspriority31;
    String primary_address_line_1;
    String city;
    String zip;
    String state_code;
    String latitude;
    String longitude;
    String borough;
    String addtl_info1;

    public SchoolData(String dbn, String school_name, String boro, String overview_paragraph, String academicopportunities1, String academicopportunities2, String ell_programs, String neighborhood, String building_code,
                      String location, String phone_number, String fax_number, String school_email, String website, String subway, String bus, String grades2018, String finalgrades, String total_students, String extracurricular_activities,
                      String school_sports, String attendance_rate, String pct_stu_enough_variety, String pct_stu_safe, String school_accessibility_description, String directions1, String requirement1_1, String requirement2_1,
                      String requirement3_1, String requirement4_1, String requirement5_1, String program1, String interest1, String admissionspriority11, String admissionspriority21, String admissionspriority31, String primary_address_line_1,
                      String city, String zip, String state_code, String latitude, String longitude, String borough, String eligibility1, String addtl_info1) {
        this.dbn = dbn;
        this.school_name = school_name;
        this.boro = boro;
        this.overview_paragraph = overview_paragraph;
        this.academicopportunities1 = academicopportunities1;
        this.academicopportunities2 = academicopportunities2;
        this.ell_programs = ell_programs;
        this.neighborhood = neighborhood;
        this.building_code = building_code;
        this.location = location;
        this.phone_number = phone_number;
        this.fax_number = fax_number;
        this.school_email = school_email;
        this.website = website;
        this.subway = subway;
        this.bus = bus;
        this.grades2018 = grades2018;
        this.finalgrades = finalgrades;
        this.total_students = total_students;
        this.extracurricular_activities = extracurricular_activities;
        this.school_sports = school_sports;
        this.attendance_rate = attendance_rate;
        this.pct_stu_enough_variety = pct_stu_enough_variety;
        this.pct_stu_safe = pct_stu_safe;
        this.school_accessibility_description = school_accessibility_description;
        this.directions1 = directions1;
        this.requirement1_1 = requirement1_1;
        this.requirement2_1 = requirement2_1;
        this.requirement3_1 = requirement3_1;
        this.requirement4_1 = requirement4_1;
        this.requirement5_1 = requirement5_1;
        this.program1 = program1;
        this.interest1 = interest1;
        this.admissionspriority11 = admissionspriority11;
        this.admissionspriority21 = admissionspriority21;
        this.admissionspriority31 = admissionspriority31;
        this.primary_address_line_1 = primary_address_line_1;
        this.city = city;
        this.zip = zip;
        this.state_code = state_code;
        this.latitude = latitude;
        this.longitude = longitude;
        this.borough = borough;
        this.eligibility1 = eligibility1;
        this.addtl_info1 = addtl_info1;
    }

    public SchoolData(String school_name, String overview_paragraph, String dbn, String eligibility1, String interest1, String addtl_info1,
                      String admissionspriority11, String admissionspriority21, String finalgrades, String total_students,
                      String extracurricular_activities){
        this.school_name = school_name;
        this.overview_paragraph = overview_paragraph;
        this.dbn = dbn;
        this.eligibility1 = eligibility1;
        this.interest1 = interest1;
        this.addtl_info1 = addtl_info1;
        this.admissionspriority11 = admissionspriority11;
        this.admissionspriority21 = admissionspriority21;
        this.finalgrades = finalgrades;
        this.total_students = total_students;
        this.extracurricular_activities = extracurricular_activities;
    }

    public String getDbn() {
        return dbn;
    }

    public void setDbn(String dbn) {
        this.dbn = dbn;
    }

    public String getSchool_name() {
        return school_name;
    }

    public void setSchool_name(String school_name) {
        this.school_name = school_name;
    }

    public String getBoro() {
        return boro;
    }

    public void setBoro(String boro) {
        this.boro = boro;
    }

    public String getOverview_paragraph() {
        return overview_paragraph;
    }

    public void setOverview_paragraph(String overview_paragraph) {
        this.overview_paragraph = overview_paragraph;
    }

    public String getAcademicopportunities1() {
        return academicopportunities1;
    }

    public void setAcademicopportunities1(String academicopportunities1) {
        this.academicopportunities1 = academicopportunities1;
    }

    public String getAcademicopportunities2() {
        return academicopportunities2;
    }

    public void setAcademicopportunities2(String academicopportunities2) {
        this.academicopportunities2 = academicopportunities2;
    }

    public String getEll_programs() {
        return ell_programs;
    }

    public void setEll_programs(String ell_programs) {
        this.ell_programs = ell_programs;
    }

    public String getNeighborhood() {
        return neighborhood;
    }

    public void setNeighborhood(String neighborhood) {
        this.neighborhood = neighborhood;
    }

    public String getBuilding_code() {
        return building_code;
    }

    public void setBuilding_code(String building_code) {
        this.building_code = building_code;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getFax_number() {
        return fax_number;
    }

    public void setFax_number(String fax_number) {
        this.fax_number = fax_number;
    }

    public String getSchool_email() {
        return school_email;
    }

    public void setSchool_email(String school_email) {
        this.school_email = school_email;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getSubway() {
        return subway;
    }

    public void setSubway(String subway) {
        this.subway = subway;
    }

    public String getBus() {
        return bus;
    }

    public void setBus(String bus) {
        this.bus = bus;
    }

    public String getGrades2018() {
        return grades2018;
    }

    public void setGrades2018(String grades2018) {
        this.grades2018 = grades2018;
    }

    public String getFinalgrades() {
        return finalgrades;
    }

    public void setFinalgrades(String finalgrades) {
        this.finalgrades = finalgrades;
    }

    public String getTotal_students() {
        return total_students;
    }

    public void setTotal_students(String total_students) {
        this.total_students = total_students;
    }

    public String getExtracurricular_activities() {
        return extracurricular_activities;
    }

    public void setExtracurricular_activities(String extracurricular_activities) {
        this.extracurricular_activities = extracurricular_activities;
    }

    public String getSchool_sports() {
        return school_sports;
    }

    public void setSchool_sports(String school_sports) {
        this.school_sports = school_sports;
    }

    public String getAttendance_rate() {
        return attendance_rate;
    }

    public void setAttendance_rate(String attendance_rate) {
        this.attendance_rate = attendance_rate;
    }

    public String getPct_stu_enough_variety() {
        return pct_stu_enough_variety;
    }

    public void setPct_stu_enough_variety(String pct_stu_enough_variety) {
        this.pct_stu_enough_variety = pct_stu_enough_variety;
    }

    public String getPct_stu_safe() {
        return pct_stu_safe;
    }

    public void setPct_stu_safe(String pct_stu_safe) {
        this.pct_stu_safe = pct_stu_safe;
    }

    public String getSchool_accessibility_description() {
        return school_accessibility_description;
    }

    public void setSchool_accessibility_description(String school_accessibility_description) {
        this.school_accessibility_description = school_accessibility_description;
    }

    public String getDirections1() {
        return directions1;
    }

    public void setDirections1(String directions1) {
        this.directions1 = directions1;
    }

    public String getRequirement1_1() {
        return requirement1_1;
    }

    public void setRequirement1_1(String requirement1_1) {
        this.requirement1_1 = requirement1_1;
    }

    public String getRequirement2_1() {
        return requirement2_1;
    }

    public void setRequirement2_1(String requirement2_1) {
        this.requirement2_1 = requirement2_1;
    }

    public String getRequirement3_1() {
        return requirement3_1;
    }

    public void setRequirement3_1(String requirement3_1) {
        this.requirement3_1 = requirement3_1;
    }

    public String getRequirement4_1() {
        return requirement4_1;
    }

    public void setRequirement4_1(String requirement4_1) {
        this.requirement4_1 = requirement4_1;
    }

    public String getRequirement5_1() {
        return requirement5_1;
    }

    public void setRequirement5_1(String requirement5_1) {
        this.requirement5_1 = requirement5_1;
    }

    public String getProgram1() {
        return program1;
    }

    public void setProgram1(String program1) {
        this.program1 = program1;
    }

    public String getInterest1() {
        return interest1;
    }

    public void setInterest1(String interest1) {
        this.interest1 = interest1;
    }

    public String getAdmissionspriority11() {
        return admissionspriority11;
    }

    public void setAdmissionspriority11(String admissionspriority11) {
        this.admissionspriority11 = admissionspriority11;
    }

    public String getAdmissionspriority21() {
        return admissionspriority21;
    }

    public void setAdmissionspriority21(String admissionspriority21) {
        this.admissionspriority21 = admissionspriority21;
    }

    public String getAdmissionspriority31() {
        return admissionspriority31;
    }

    public void setAdmissionspriority31(String admissionspriority31) {
        this.admissionspriority31 = admissionspriority31;
    }

    public String getPrimary_address_line_1() {
        return primary_address_line_1;
    }

    public void setPrimary_address_line_1(String primary_address_line_1) {
        this.primary_address_line_1 = primary_address_line_1;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getState_code() {
        return state_code;
    }

    public void setState_code(String state_code) {
        this.state_code = state_code;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getBorough() {
        return borough;
    }

    public void setBorough(String borough) {
        this.borough = borough;
    }

    public String getEligibility1() {
        return eligibility1;
    }

    public void setEligibility1(String eligibility1) {
        this.eligibility1 = eligibility1;
    }

    public String getAddtl_info1() {
        return addtl_info1;
    }

    public void setAddtl_info1(String addtl_info1) {
        this.addtl_info1 = addtl_info1;
    }
}
