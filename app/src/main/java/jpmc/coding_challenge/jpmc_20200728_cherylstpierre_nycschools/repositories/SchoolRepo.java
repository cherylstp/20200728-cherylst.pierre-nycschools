package jpmc.coding_challenge.jpmc_20200728_cherylstpierre_nycschools.repositories;

import android.app.Application;
import android.util.Log;
import java.util.List;

import jpmc.coding_challenge.jpmc_20200728_cherylstpierre_nycschools.R;
import jpmc.coding_challenge.jpmc_20200728_cherylstpierre_nycschools.models.SATData;
import jpmc.coding_challenge.jpmc_20200728_cherylstpierre_nycschools.models.SchoolData;
import jpmc.coding_challenge.jpmc_20200728_cherylstpierre_nycschools.networking.RetrofitServiceGenerator;
import jpmc.coding_challenge.jpmc_20200728_cherylstpierre_nycschools.networking.Routes;
import jpmc.coding_challenge.jpmc_20200728_cherylstpierre_nycschools.networking.callbacks.GenericCallback;
import jpmc.coding_challenge.jpmc_20200728_cherylstpierre_nycschools.networking.callbacks.GenericListCallback;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SchoolRepo {
    private static SchoolRepo instance = null;
    private Routes routes;
    private String genericError;
    private static final String TAG = "SchoolRepo";

    // Used to keep this a singleton, so other classes cannot instantiate
    private SchoolRepo(){}

    // This will only ever allow one instance of this class to be used
    public static synchronized SchoolRepo getInstance(Application app){
        if(instance == null){
            instance = new SchoolRepo();
            instance.routes = RetrofitServiceGenerator.createService(Routes.class);
            instance.genericError = app.getString(R.string.generic_error);
        }
        return instance;
    }

    public void getAllSchoolData(final GenericListCallback callback){
        routes.getAllSchools().enqueue(new Callback<List<SchoolData>>() {
            @Override
            public void onResponse(Call<List<SchoolData>> call, Response<List<SchoolData>> response) {
                // Here, I would typically handle the different response codes coming back from the API
                // I usually check for 200, 404, 403, or other codes that I know the endpoint could return
                // Since I'm not sure what codes this specific endpoint returns, we'll just
                // pass the response to the callback
                callback.handleListResponse(response.body(), null);
            }

            @Override
            public void onFailure(Call<List<SchoolData>> call, Throwable t) {
                Log.e(TAG, t.toString());
                callback.handleListResponse(null, genericError);
            }
        });
    }

    public void getAllSATData(String dbn, final GenericCallback callback){
        routes.getAllSATData(dbn).enqueue(new Callback<List<SATData>>() {
            @Override
            public void onResponse(Call<List<SATData>> call, Response<List<SATData>> response) {
                // Here, I would typically handle the different response codes coming back from the API
                // I usually check for 200, 404, 403, or other codes that I know the endpoint could return
                // Since I'm not sure what codes this specific endpoint returns, we'll just
                // pass the response to the callback
                callback.handleResponse(response.body(), null);
            }

            @Override
            public void onFailure(Call<List<SATData>> call, Throwable t) {
                Log.e(TAG, t.toString());
                callback.handleResponse(null, genericError);
            }
        });
    }

}
