package jpmc.coding_challenge.jpmc_20200728_cherylstpierre_nycschools;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * In all honesty, we don't use unit testing at my current job, as QA typically does testing for us.
 * I have been looking into how to write unit tests (specifically for testing database migrations)
 * to make it easier for myself to test edge cases, and I am confident that this is something I can
 * pick up and implement in the future!
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() {
        assertEquals(4, 2 + 2);
    }
}